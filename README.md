# Dmat-Latex

[![estado del pipeline](https://gitlab.com/fralfaro/dmat-latex/badges/main/pipeline.svg)](https://gitlab.com/fralfaro/dmat-latex/-/commits/main)
<a href="https://gitlab.com/fralfaro/dmat-latex/-/jobs/artifacts/main/browse?job=generate_pdf"><img alt="Enlace a la Documentación" src="https://img.shields.io/badge/pdf-enlace-brightgreen"></a>


<img src="img/logo-readme.png" alt="" align="center" width="200"/>

Plantillas LaTeX: Tarea, Beamer.

> **Nota**: Para más detalles, consultar el siguiente [link](https://www.alejandriaicm.com/home).

## ¿Dónde?
[Haz clic aquí](https://gitlab.com/fralfaro/dmat-latex/-/jobs/artifacts/main/browse?job=generate_pdf) para explorar la última versión de los archivos.

## ¿Por qué?
Hice este proyecto tiene como objetivo:
  - Versionar y Automatizar el proceso de actualizar los archivos.
  - Aprender más sobre Integración Continua.
  - Aprender cómo utilizar [Gitlab CI/CD Pipelines](https://about.gitlab.com/product/continuous-integration/).

